package com.emids.health.insurance.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.emids.health.insurance.models.CurrentHealth;
import com.emids.health.insurance.models.Habits;
import com.emids.health.insurance.models.HealthDetails;
import com.emids.health.insurance.utils.HealthUtil;

class HealthUtilTest {

	HealthUtil healthUtil = new HealthUtil();
	
   // test method to add two values
   @Test
   public void testPremiumCalculator(){
	   HealthDetails healthDeatails = new  HealthDetails();
	   healthDeatails.setName("Norman Gomes");
	   healthDeatails.setAge(34);
	   healthDeatails.setGender("Male");
		
	   CurrentHealth currentHealth = new CurrentHealth();
	   currentHealth.setBloodPressure(false);
	   currentHealth.setBloodSugar(false);
	   currentHealth.setHypertension(false);
	   currentHealth.setOverweight(true);
		
	   Habits habits = new Habits();
	   habits.setAlcohol(true);
	   habits.setDailyExercise(true);
	   habits.setDrugs(false);
	   habits.setSmoking(false);
		
	   healthDeatails.setHealth(currentHealth);
	   healthDeatails.setHabits(habits);
	   
	   int actualPremium = 6736;
       int expectedPremium = healthUtil.premiumCalculator(healthDeatails);
      
      assertEquals(actualPremium, expectedPremium);
   }
   
   @Test
   public void testPremiumCalculatorTrue(){
	   HealthDetails healthDeatails = new  HealthDetails();
	   healthDeatails.setName("Norman Gomes");
	   healthDeatails.setAge(41);
	   healthDeatails.setGender("Male");
		
	   CurrentHealth currentHealth = new CurrentHealth();
	   currentHealth.setBloodPressure(true);
	   currentHealth.setBloodSugar(true);
	   currentHealth.setHypertension(true);
	   currentHealth.setOverweight(true);
		
	   Habits habits = new Habits();
	   habits.setAlcohol(true);
	   habits.setDailyExercise(true);
	   habits.setDrugs(true);
	   habits.setSmoking(true);
		
	   healthDeatails.setHealth(currentHealth);
	   healthDeatails.setHabits(habits);
	   
	   int actualPremium = 9554;
       int expectedPremium = healthUtil.premiumCalculator(healthDeatails);
      
      assertEquals(actualPremium, expectedPremium);
   }
   
   @Test
   public void testPremiumCalculatorFalse(){
	   HealthDetails healthDeatails = new  HealthDetails();
	   healthDeatails.setName("Norman Gomes");
	   healthDeatails.setAge(37);
	   healthDeatails.setGender("Male");
		
	   CurrentHealth currentHealth = new CurrentHealth();
	   currentHealth.setBloodPressure(false);
	   currentHealth.setBloodSugar(false);
	   currentHealth.setHypertension(false);
	   currentHealth.setOverweight(false);
		
	   Habits habits = new Habits();
	   habits.setAlcohol(false);
	   habits.setDailyExercise(false);
	   habits.setDrugs(false);
	   habits.setSmoking(false);
		
	   healthDeatails.setHealth(currentHealth);
	   healthDeatails.setHabits(habits);
	   
	   int actualPremium = 6411;
       int expectedPremium = healthUtil.premiumCalculator(healthDeatails);
      
      assertNotEquals(actualPremium, expectedPremium);
   }

}
