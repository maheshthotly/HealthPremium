package com.emids.health.insurance.utils;

import com.emids.health.insurance.models.HealthDetails;

public class HealthUtil {

	public int premiumCalculator(HealthDetails healthDeatils) {

		int basePremium = 5000;
		int totalPremium = basePremium;
		int age = healthDeatils.getAge();
		String gender = healthDeatils.getGender();

		if (age < 18) {
			totalPremium = basePremium;
		} else if (age >= 18 && age < 25) {
			totalPremium = percentageCalculator(totalPremium, 10, true);
			;
		} else if (age >= 25 && age < 30) {
			totalPremium = percentageCalculator(totalPremium, 22, true);
			;
		} else if (age >= 30 && age < 35) {
			totalPremium = percentageCalculator(totalPremium, 35, true);
			;
		} else if (age >= 35 && age < 40) {
			totalPremium = percentageCalculator(totalPremium, 48, true);
			;
		} else if (age > 40) {
			totalPremium = percentageCalculator(totalPremium, 60, true);
			;
		}

		if ("Male".equalsIgnoreCase(gender)) {
			totalPremium = percentageCalculator(totalPremium, 2, true);
		}

		// Health check
		if (healthDeatils.getHealth().isBloodPressure()) {
			totalPremium = percentageCalculator(totalPremium, 1, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 1, false);
		}
		if (healthDeatils.getHealth().isBloodSugar()) {
			totalPremium = percentageCalculator(totalPremium, 1, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 1, false);
		}
		if (healthDeatils.getHealth().isHypertension()) {
			totalPremium = percentageCalculator(totalPremium, 1, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 1, false);
		}
		if (healthDeatils.getHealth().isOverweight()) {
			totalPremium = percentageCalculator(totalPremium, 1, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 1, false);
		}

		// habits check
		if (healthDeatils.getHabits().isAlcohol()) {
			totalPremium = percentageCalculator(totalPremium, 3, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 3, false);
		}
		if (healthDeatils.getHabits().isDailyExercise()) {
			totalPremium = percentageCalculator(totalPremium, 3, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 3, false);
		}
		if (healthDeatils.getHabits().isDrugs()) {
			totalPremium = percentageCalculator(totalPremium, 3, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 3, false);
		}
		if (healthDeatils.getHabits().isSmoking()) {
			totalPremium = percentageCalculator(totalPremium, 3, true);
		} else {
			totalPremium = percentageCalculator(totalPremium, 3, false);
		}

		return totalPremium;

	}

	public int percentageCalculator(int premium, int percentage, boolean flag) {
		if (flag) {
			premium = premium + (premium * percentage) / 100;
		} else {
			premium = premium - (premium * percentage) / 100;
		}
		return premium;
	}

}
