package com.emids.health.insurance.utils;

import com.emids.health.insurance.models.CurrentHealth;
import com.emids.health.insurance.models.Habits;
import com.emids.health.insurance.models.HealthDetails;

public class HealthUtilMain {

	public static void main(String[] args) {
		HealthUtil healthUtil = new HealthUtil();
		HealthDetails healthDeatails = new HealthDetails();
		healthDeatails.setName("Norman Gomes");
		healthDeatails.setAge(34);
		healthDeatails.setGender("Male");

		CurrentHealth currentHealth = new CurrentHealth();
		currentHealth.setBloodPressure(false);
		currentHealth.setBloodSugar(false);
		currentHealth.setHypertension(false);
		currentHealth.setOverweight(true);

		Habits habits = new Habits();
		habits.setAlcohol(true);
		habits.setDailyExercise(true);
		habits.setDrugs(false);
		habits.setSmoking(false);

		healthDeatails.setHealth(currentHealth);
		healthDeatails.setHabits(habits);

		int totalPremium = healthUtil.premiumCalculator(healthDeatails);
		System.out.println("Health Insurance Premium for Mr." + healthDeatails.getName() + ":" + totalPremium);
	}

}
