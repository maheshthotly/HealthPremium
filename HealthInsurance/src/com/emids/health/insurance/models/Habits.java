package com.emids.health.insurance.models;

public class Habits {
	
	private boolean smoking;
	private boolean alcohol;
	private  boolean dailyExercise;
	private  boolean drugs;
	/**
	 * @return the smoking
	 */
	public boolean isSmoking() {
		return smoking;
	}
	/**
	 * @param smoking the smoking to set
	 */
	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}
	/**
	 * @return the alcohol
	 */
	public boolean isAlcohol() {
		return alcohol;
	}
	/**
	 * @param alcohol the alcohol to set
	 */
	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}
	/**
	 * @return the dailyExercise
	 */
	public boolean isDailyExercise() {
		return dailyExercise;
	}
	/**
	 * @param dailyExercise the dailyExercise to set
	 */
	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	/**
	 * @return the drugs
	 */
	public boolean isDrugs() {
		return drugs;
	}
	/**
	 * @param drugs the drugs to set
	 */
	public void setDrugs(boolean drugs) {
		this.drugs = drugs;
	}
	
	
	
	
}
