package com.emids.health.insurance.models;

public class HealthDetails {
	
	private String name;
	private String gender;
	private int age;
	private CurrentHealth health;
	private  Habits habits;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the health
	 */
	public CurrentHealth getHealth() {
		return health;
	}
	/**
	 * @param health the health to set
	 */
	public void setHealth(CurrentHealth health) {
		this.health = health;
	}
	/**
	 * @return the habits
	 */
	public Habits getHabits() {
		return habits;
	}
	/**
	 * @param habits the habits to set
	 */
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
	

}
