package com.emids.health.insurance.models;

public class CurrentHealth {
	
	private  boolean hypertension;
	private  boolean bloodPressure;
	private  boolean bloodSugar;
	private  boolean overweight;
	/**
	 * @return the hypertension
	 */
	public boolean isHypertension() {
		return hypertension;
	}
	/**
	 * @param hypertension the hypertension to set
	 */
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	/**
	 * @return the bloodPressure
	 */
	public boolean isBloodPressure() {
		return bloodPressure;
	}
	/**
	 * @param bloodPressure the bloodPressure to set
	 */
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	/**
	 * @return the bloodSugar
	 */
	public boolean isBloodSugar() {
		return bloodSugar;
	}
	/**
	 * @param bloodSugar the bloodSugar to set
	 */
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	/**
	 * @return the overweight
	 */
	public boolean isOverweight() {
		return overweight;
	}
	/**
	 * @param overweight the overweight to set
	 */
	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	
	
	

}
